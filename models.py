from contextlib import contextmanager
from datetime import datetime

import sqlalchemy as sa
import sqlalchemy.orm as sa_orm
from sqlalchemy.ext.declarative import declarative_base


DATETIME_STR_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
"""The string format for a UTC DateTime."""

# Setup the base to use for all of the Models
Base = declarative_base()


def initialize_db(engine, drop_all=False):
    """Initialize the data base with all models.

    Args:
        engine: The sqlalchemy engine to use
        drop_all: Whether or not to drop all of the tables. Default False.
    """
    if drop_all:
        Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)


@contextmanager
def transaction(session):
    """A contextmanager for that handles rolling back in case of failure.

    A contextmanager for a transaction such that it will try to commit what is
    being managed, but ensure that the transaction is rolled back in case of
    an exception. The Exception will then be re-raised.

    Args:
        session: The SQLAlchemy session
    Returns:
        None
    Raises:
        Exception: Any exception that may be raised by the context being
            managed.
    """
    try:
        yield
        session.commit()
    except:
        session.rollback()
        raise


class User(Base):
    """The model for a user."""
    __tablename__ = 'users'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = sa.Column(sa.Integer, primary_key=True)
    first_name = sa.Column(sa.String, nullable=False)
    last_name = sa.Column(sa.String, nullable=False)
    email = sa.Column(sa.String, nullable=False, unique=True, index=True)
    password = sa.Column(sa.String(60), nullable=False)
    added = sa.Column(sa.DateTime, nullable=False, default=datetime.utcnow)

    items = sa_orm.relationship('Item', order_by='Item.id', backref='user')
    files = sa_orm.relationship('File', order_by='File.id', backref='user')

    def __init__(self, id=None, first_name=None, last_name=None, email=None,
        password=None, added=None):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password
        if added:
            self.added = added

    def get_id(self, type='hex'):
        """Get the user id. Required by Flask-Login."""
        return unicode(self.id)

    def is_authenticated(self):
        """Whether or not the user is authenticated. Required by Flask-Login.

        In general this method should just return True unless the object
        represents a user that should not be allowed to authenticate for some
        reason.
        """
        return True

    def is_active(self):
        """Whether or not the user is active. Required by Flask-Login."""
        return True

    def is_anonymous(self):
        """Whether or not the user is #anonymous. Required by Flask-Login."""
        return False

    def to_dict(self):
        """Returns a dictionary containing the attributes."""
        return dict(id=self.id,
                    first_name=self.first_name,
                    last_name=self.last_name,
                    email=self.email,
                    added=self.added.strftime(DATETIME_STR_FORMAT))


class Item(Base):
    """The model for a item."""
    __tablename__ = 'items'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = sa.Column(sa.Integer, primary_key=True)
    creator_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'),
                            nullable=False)
    description = sa.Column(sa.String, nullable=False)
    added = sa.Column(sa.DateTime, nullable=False, default=datetime.utcnow)
    updated = sa.Column(sa.DateTime, nullable=False, default=datetime.utcnow,
                        onupdate=datetime.utcnow)

    files = sa_orm.relationship('ItemFile', order_by='ItemFile.added')

    def __init__(self, id=None, creator_id=None, description=None, added=None,
                 updated=datetime.utcnow()):
        self.id = id
        self.creator_id = creator_id
        self.description = description
        if added:
            self.added = added
        if updated:
            self.updated = updated

    def to_dict(self):
        """Returns a dictionary containing the attributes."""
        return dict(id=self.id,
                    creator_id=self.creator_id,
                    description=self.description,
                    added=self.added.strftime(DATETIME_STR_FORMAT),
                    updated=self.updated.strftime(DATETIME_STR_FORMAT))


class File(Base):
    """The model for a file."""
    __tablename__ = 'files'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = sa.Column(sa.Integer, primary_key=True)
    creator_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'),
                            nullable=False)
    file_name = sa.Column(sa.String, nullable=False)
    file_path = sa.Column(sa.String, nullable=False)
    added = sa.Column(sa.DateTime, nullable=False, default=datetime.utcnow)

    def __init__(self, id=None, creator_id=None, file_name=None,
                file_path=None, added=None):
        self.id = id
        self.creator_id = creator_id
        self.file_name = file_name
        self.file_path = file_path
        if added:
            self.added = added

    def to_dict(self):
        """Returns a dictionary containing the attributes."""
        return dict(id=self.id,
                    creator_id=self.creator_id,
                    file_name=self.file_name,
                    file_path=self.file_path,
                    added=self.added.strftime(DATETIME_STR_FORMAT))


class ItemFile(Base):
    """The model for the mapping between items and files."""
    __tablename__ = 'item_files'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    item_id = sa.Column(sa.Integer, sa.ForeignKey('items.id'), nullable=False,
                        primary_key=True)
    file_id = sa.Column(sa.Integer, sa.ForeignKey('files.id'), nullable=False,
                        primary_key=True)
    added = sa.Column(sa.DateTime, nullable=False, default=datetime.utcnow)

    item = sa_orm.relationship('Item', order_by='Item.id', uselist=False)
    file = sa_orm.relationship('File', order_by='File.id', uselist=False)

    def __init__(self, item_id=None, file_id=None, added=None):
        self.item_id = item_id
        self.file_id = file_id
        if added:
            self.added = added

    def to_dict(self):
        """Returns a dictionary containing the attributes."""
        return dict(item_id=self.item_id,
                    file_id=self.file_id,
                    added=self.added.strftime(DATETIME_STR_FORMAT))
