var Config = {
    environments: {
        local: {
            api: ''
        },
        dev: {
            api: ''
        },
        production: {
            api: ''
        }
    },

    get: function(key) {
        var env;
        switch (window.location.hostname) {
            case "localhost":
            case "127.0.0.1":
                env = 'local';
                break;
            case "devserver.com":
                env = 'dev';
                break;
            case "productionserver.com":
                env = 'production';
                break;
            default:
                throw('Unknown environment: ' + window.location.hostname);
        }
        return this.environments[env][key];
    }
};

(function() {

    var proxiedSync = Backbone.sync;

    Backbone.sync = function(method, model, options) {
        options || (options = {});

        if (!options.crossDomain) {
            options.crossDomain = true;
        }

        if (!options.xhrFields) {
            options.xhrFields = {withCredentials:true};
        }

        return proxiedSync(method, model, options);
    };

})();