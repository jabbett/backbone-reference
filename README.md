Backbone.js Reference App
=========================

This is a reference app for testing important features of [Backbone.js](href="http://backbonejs.org/)

- models & collections

- model view binding

- subviews

...and important features of typical webapps using Backbone.js:

- application configuration

- authentication

- session management

- AJAX file uploads

It includes a Python/[Flask](http://flask.pocoo.org)/[SQLAlchemy](http://www.sqlalchemy.org) web service (using an SQLite database) for easy out-of-the-box testing. This web service does the following:

- Uses the Flask microframework to create a REST API

- Customized HTTP error handlers

- Uses Flask-Login to handle authentication

- Uses Flask-Uploads to handle file uploads

- File names are randomized to ensure no duplicate names

- Passlib is used to securely hash user passwords

- SQLAlchemy is used to manage all models and database connections (This means you can modify this app to use MySQL, PostgreSQL, etc) by changing only one line of code.

This app requires Python 2.7.3.

Installation
------------

After cloning the repository, you will need to make sure that the appropriate Python libraries are installed in your (virtual) environment. To get all the requirements at once, simply load them from the PIP requirements file.

    pip install -r requirements.txt


Configuration
-------------

If you are on a *NIX system (Linux, Unix, OS X) then there should be no configuration necessary.
However, if you are using a Windows computer then you will need to open up the app.py file and modify the APP_DIR to a valid directory on your machine.

By default, all data (including the SQLite database) is stored in

    /var/tmp/backbone-reference

Running
-------

To run the app for the first, simply start the server using
    
    python app.py init

This will initialize the database and add a default user with username "albert@einstein.com" and password "pass". You can then visit http://localhost:5000/

The second time you start the server, you no longer need to tell it to initialize itself (unless you want to clear everything), so you can just run it as follows:

    python app.py
