Application = Backbone.Router.extend({

    el: $('#mainContainer'),

    routes: {
        ""            : "home",
        "signup"      : "signup",
        "login"       : "login",
        "editItem(/:itemId)"    : "editItem"
    },

    currentView: null,


    /**
     * This removes the main view and switches it with the new main view.
     * @param view
     */
    switchView: function(view) {

        // TODO: Does each view need to be responsible for destroying its subviews?

        if (this.currentView) {
            // This is where we chainsaw the zombie view :)
            this.currentView.remove();
        }

        this.el.html(view.el);

        // not rendering here, since some views are rendered upon fetch(), but...
        // TODO: revisit this when we're done

        // TODO: maybe apply a "loading" scrim over page?

        this.currentView = view;

    },

    initialize: function() {
        this.navigationView = new NavigationView();
        $('#mainNavigation').html(this.navigationView.render().el);
    },

    login: function() {
        this.setTitle('Log In');

        var loginView = new LoginView();
        this.switchView(loginView);

        loginView.render();
    },

    signup: function(plan) {

        this.setTitle('Sign Up');

        signupView = new SignUpView();
        this.switchView(signupView);

        signupView.render();
    },

    home: function() {

        this.setTitle('Home');
        var self = this;

        Session.required(function() {

            var items = new ItemCollection();

            var homeView = new HomeView({
                collection: items
            });

            self.switchView(homeView);

            items.fetch();

        });

    },

    editItem: function(itemId) {
        window.console && console.log('editItem: ' + itemId);
        var self = this;

        Session.required(function() {

            var item = new Item();

            var editItemView = new EditItemView({
                model: item
            });

            self.switchView(editItemView);

            if (itemId != null) {
                self.setTitle('Edit Item');
                editItemView.model.set({id: itemId});
                editItemView.model.fetch({
                    success: function() {
                        editItemView.render();
                    }
                });
            } else {
                self.setTitle('New Item');
                editItemView.render();
            }

        });
    },

    setTitle: function(title) {
        document.title = title + ' - Backbone Reference Application';
    }

});

var application = new Application();
Backbone.history.start();