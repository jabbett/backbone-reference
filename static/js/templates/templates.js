Handlebars.getTemplate = function(name) {
    if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
        $.ajax({
            url : '/static/js/templates/' + name + '.handlebars',
            datatype: 'text/javascript',
            success : function(response, status, jqXHR) {
                if (Handlebars.templates === undefined) {
                    Handlebars.templates = {};
                }
                Handlebars.templates[name] = Handlebars.compile(jqXHR.responseText);
            },
            async : false
        });
    }
    return Handlebars.templates[name];
};

Handlebars.loadPartial = function(name) {
    if (!(name in Handlebars.partials)) {
        $.ajax({
            url : '/static/js/templates/partials/' + name + '.handlebars',
            success : function(data) {
                Handlebars.registerPartial(name,  data);
            },
            async : false
        })
    }
};