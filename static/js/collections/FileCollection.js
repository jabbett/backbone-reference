FileCollection = Backbone.Collection.extend({
    model: UserFile,
    url: function() {
        return Config.get('api') + '/user/item/' + this.itemId + '/files';
    },
    initialize: function(models, options) {
        this.itemId = options.itemId;
    },
    parse: function(response) {
        return response.files;
    }
});