LoginView = Backbone.View.extend({

    events: { 'submit form' : 'login' },

    template: Handlebars.getTemplate('login'),

    initialize: function() {
        _.bindAll(this, 'render');
    },

    render: function(eventName) {

        $(this.el).html(this.template());

        // these were in initialize, but they don't exist at init time
        this.form = this.$('form');
        this.formMessage = this.$('.form-message');
        this.emailField = this.$('input[name=email]');
        this.passwordField = this.$('input[name=password]');
        this.submitButton = this.$('button');

        return this;
    },

    login: function() {

        if (this.submitButton.hasClass('disabled') && !(this.form.data('user-authorized') === true)) {
            return false;
        } else {
            this.submitButton.addClass('disabled');
        }

        this.formMessage.slideUp();

        var self = this;

        Session.login(
            this.emailField.val(),
            this.passwordField.val(),
            function() { self.loginSuccess() },
            function(xhr, status, error) { self.loginFailure(xhr, status, error); }
        );

        // why do we do this?
        return (this.form.data('user-authorized') === true);

    },

    loginSuccess: function (user) {
        // why do we do this?
        this.form.data('user-authorized', true);

        // TODO: if user was redirected to login from elsewhere, send user there

        // default: go to the dashboard
        application.navigate('#/');
    },

    loginFailure: function (xhr, status, error) {

        if (xhr.status == 400 || xhr.status == 401) {
            // authentication failed
            this.formMessage.hide().slideDown();
            this.emailField.focus();
            this.submitButton.removeClass('disabled');
        } else {
            alert('unexpected response: ' + xhr.status + ' ' + status);
        }

    }

});