/**
 * The Session object.
 * @type {Object}
 */
Session = {};

/**
 *
 * @type User
 */
Session.currentUser = null;

/**
 *
 * @param email
 * @param password
 * @param onSuccess
 * @param onError
 */
Session.login = function(email, password, onSuccess, onError) {
    $.ajax({
        type: 'POST',
        url: Config.get('api') + '/login',
        contentType: "application/json",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        data: JSON.stringify({
            email: email,
            password: password
        }),
        success: function(data) {
            Session.setCurrentUser(data);
            onSuccess.call(Session.currentUser);
        },
        error: onError
    })
};

/**
 *
 */
Session.logout = function() {
    $.ajax({
        type: 'GET',
        url: Config.get('api') + '/logout',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function() {
            Session.setCurrentUser(null);
            application.navigate('#/login');
        },
        error: function(xhr, status, error) {
            window.console && console.log('Unexpected error logging out: ' + status);
        }
    })

};

/**
 *
 * @param onSuccess
 */
Session.required = function(onSuccess) {

    if (Session.currentUser == null || !Session.currentUser.id) {
        $.ajax({
            type: 'GET',
            url: Config.get('api') + '/user',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                Session.setCurrentUser(data);
                onSuccess.call(Session.currentUser);
            },
            error: function(xhr, status, error) {
                if (xhr.status == 401) {
                    application.navigate('#/login');
                } else {
                    Session.error("Sorry, the website seems to be having some issues. (If you're really curious, we had a problem recreating your client session.) If you see this message again, please click Feedback to be in touch.");
                    window.console && console.log('Unexpected error refreshing session: ' + status);
                }
            }
        })
    } else {
        // client session already created, just call the success method
        onSuccess.call();
    }

};

/**
 * In general, you won't want to call this unless you've already called Session.required
 * @param onSuccess
 * @return User
 */
Session.getCurrentUser = function() {
    if (Session.currentUser == null) {
        Session.currentUser = new User();
    }
    return Session.currentUser;
}

/**
 *
 * @param data
 */
Session.setCurrentUser = function(data) {

    if (data == null) {
        // clear out the current user
        Session.getCurrentUser().clear();
    } else {
        Session.getCurrentUser().set(data);
    }

}

/**
 *
 * @param message
 */
Session.error = function(message) {
    $('#errorModalMessage').text(message);
    $('#errorModal').modal('show');
}